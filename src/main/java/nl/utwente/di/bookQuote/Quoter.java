package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn)
    {
        double a;
        switch (isbn){
            case "1":
               a = 10.0;
                break;
            case "2":
                a = 45.0;
                break;
            case "3":
                a = 20.0;
                break;
            case "4":
                a = 35.0;
                break;
            case "5":
                a = 50.0;
                break;
            default:
                a = 0;
        }

        return a;
    }
}
